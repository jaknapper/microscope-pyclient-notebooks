import numpy as np
import time
import openflexure_microscope_client
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation

plt.style.use('fivethirtyeight')

microscope = openflexure_microscope_client.find_first_microscope()
# microscope = openflexure_microscope_client.MicroscopeClient("169.254.67.213")
pos = microscope.position
starting_pos = pos.copy()

def track_sharpness():

    x_vals = []
    y_vals = []

    # start_time = time.time()

    fig = plt.figure()

    def animate(i):
        microscope.move_rel([0,0,50])
        x_vals.append(microscope.position['z'])
        time.sleep(0.5)
        y_vals.append(microscope.measure_sharpness()['sharpness'])
        
        fig = plt.plot(x_vals, y_vals)

    ani = FuncAnimation(fig, animate, interval=1)

    plt.tight_layout()
    plt.show()

track_sharpness()
plt.show()